// Top-level build file where you can add configuration options common to all sub-projects/modules.
buildscript {
    repositories {
        maven {
            url = uri("C:\\Users\\Lenovo\\Documents\\asyfa\\DexGuard-9.7.0\\lib");
        }

        dependencies {
            classpath("com.guardsquare:dexguard-gradle-plugin:9.7.0")
        }
    }
}

plugins {
    id("com.android.application") version "8.2.0" apply false
    id("org.jetbrains.kotlin.android") version "1.9.0" apply false
}